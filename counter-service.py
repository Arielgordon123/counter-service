#!flask/bin/python
from flask import Flask, request, request_started
import redis
import os

app = Flask(__name__)
version = os.getenv("SERVICE_VERSION")
hostname = os.getenv("HOSTNAME")
r = redis.Redis(host=os.getenv("REDIS_HOST"),
                password=os.getenv("REDIS_PASSWORD"))
r.setnx("global_counter", 0)
r.setnx(hostname, 0)


@app.route('/', methods=["POST", "GET"])
def index():
    if request.method == "POST":
        r.incr("global_counter", 1)
        r.incr(hostname, 1)
        return "Hmm, Plus 1 please "
    else:
        global_counter = r.get("global_counter").decode()
        local_counter = r.get(hostname).decode()
        return str(f"The sum of POST counter is: {global_counter} <BR/><BR/> The sum of local POST counter is: {local_counter} <BR/><BR/> service version: {version}")


if __name__ == '__main__':
    app.run(debug=True, port=80, host='0.0.0.0')
