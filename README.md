# counter-service

This is a simple web service that count the number of `POST` requests it has processed, and return the counter on evrey `GET` request it get.


## Local run `counter-service` with `docker`

```bash
docker build -t counter-service .

docker run -it --rm -p 8080:80 counter-service

```

Then you can start send requests to the service from another terminal

```bash
curl -X POST localhost:8080
```